# Hortonworks Spark Certification test pattern, question 5. #

*5th is to use accumulator to perform word count.*

Our Hortonworks trainer told us during the Spark workshop in Prague that in the exam    
there is an task **to count how many times word "spark" occurs in HDFS file**...Okay, let's do it!

**Input file:**

    [root@sandbox ~]# cat inputFile.txt 
    First line the word Spark
    Second line, the word Spark is here twice, Spark
    Thirs line, no spark
    Last line.
    [root@sandbox ~]# hdfs dfs -put inputFile.txt /tests/inputFile.txt

Word spark is here **4x times**.

## First solution, accumulator.

First solution and solution I'm going to use during the certification is using of accumulator.

    %spark
    def wordCounter(line: String, word: String):Int={
        val arr = line.split(" ");
        var count = 0;
        for (i <- 0 to arr.length - 1) {
            if (arr(i).trim().toLowerCase().equals(word)) {
                count = count + 1;
            }
        }
    
        return count;
    }

    val fileRDD = sc.textFile("/tests/inputFile.txt");

    var accum = sc.accumulator(0);

    fileRDD.foreach(line=>{
        accum.add(wordCounter(line, "spark")); 
    });

    println("Word spark count = "+accum.value);

Zeppelin output:

    wordCounter: (line: String, word: String)Int
    fileRDD: org.apache.spark.rdd.RDD[String] = /tests/inputFile.txt MapPartitionsRDD[473] at textFile at <console>:125
    accum: org.apache.spark.Accumulator[Int] = 0
    Word spark count = 4

## Second solution, RDD API.

The idea is:

- Let's break the file into the key,value pairs, where key is the word in a file and value is 1 if the word is "spark", 0 otherwise.
- reduce the key values with adding them.
- Filter out reduce result of a "spark" key.

Solution:

    %spark
    
    val fileRDD = sc.textFile("/tests/inputFile.txt");

    val words = fileRDD.flatMap(line=>line.split(" "));

    val pairsRDD = words.map(word=>{
       val key = word.trim().toLowerCase();      
       if (key.equals("spark")) {
           (key, 1)
       } else {
           (key, 0)
       }
    });

    val reducedRDD = pairsRDD.reduceByKey((x,y)=>x+y);

    val wordResult = reducedRDD.filter(pair=>pair._1.equals("spark")).take(1);

    println("Word spark count = "+wordResult(0)._2);

Zeppelin output:

    fileRDD: org.apache.spark.rdd.RDD[String] = /tests/inputFile.txt MapPartitionsRDD[475] at textFile at <console>:125
    words: org.apache.spark.rdd.RDD[String] = MapPartitionsRDD[476] at flatMap at <console>:127
    pairsRDD: org.apache.spark.rdd.RDD[(String, Int)] = MapPartitionsRDD[477] at map at <console>:129
    reducedRDD: org.apache.spark.rdd.RDD[(String, Int)] = ShuffledRDD[478] at reduceByKey at <console>:131
    wordResult: Array[(String, Int)] = Array((spark,4))
    Word spark count = 4

Best regards

Tomas







